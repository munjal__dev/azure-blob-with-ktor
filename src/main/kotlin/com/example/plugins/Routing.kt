package com.example.plugins

import com.azure.core.util.BinaryData
import com.azure.storage.blob.BlobClientBuilder
import com.azure.storage.blob.models.BlobAccessPolicy
import com.azure.storage.blob.models.BlobSignedIdentifier
import com.azure.storage.blob.models.PublicAccessType
import com.azure.storage.blob.models.UserDelegationKey
import com.azure.storage.blob.sas.BlobContainerSasPermission
import com.azure.storage.blob.sas.BlobSasPermission
import com.azure.storage.blob.sas.BlobServiceSasSignatureValues
import com.azure.storage.common.StorageSharedKeyCredential
import com.azure.storage.common.sas.SasProtocol
import com.example.azureclient.AzureClient
import io.ktor.http.content.*
import io.ktor.server.application.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import java.time.OffsetDateTime
import java.util.Collections


fun Application.configureRouting() {

    routing {
        post("/") {
            val storageClient = AzureClient.storageClient

            val blobContainerClient = storageClient.createBlobContainerIfNotExists("test")

            var fileDescription = ""
            var fileName = ""
            val multiPart = call.receiveMultipart()

            multiPart.forEachPart { part ->
                when(part){
                    is PartData.FormItem -> {
                        fileDescription = part.value
                    }
                    is PartData.FileItem -> {
                        fileName = part.originalFileName as String
                        var fileBytes = part.streamProvider().readAllBytes()
                        val binarydata = BinaryData.fromBytes(fileBytes)

                        val blobClient =  blobContainerClient.getBlobClient(fileName)

                        blobClient.upload(binarydata,true)



                        val sasPermission = BlobContainerSasPermission().setReadPermission(true).setWritePermission(true)
                            .setAddPermission(true)
                            .setCreatePermission(true)
                            .setDeletePermission(true);
                        val expiration = OffsetDateTime.now().plusDays(1)

                        var sasValues =  BlobServiceSasSignatureValues(expiration,sasPermission).setProtocol(SasProtocol.HTTPS_ONLY)

                        val sasToken = blobContainerClient.generateSas(sasValues)

                        call.respondText("${blobClient.blobUrl}?${sasToken}" )
                    }

                    else -> {
                        call.respondText("error")
                    }
                }
            }
        }
    }
}


