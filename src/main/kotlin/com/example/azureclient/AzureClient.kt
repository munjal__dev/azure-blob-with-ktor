package com.example.azureclient

import com.azure.core.http.ProxyOptions
import com.azure.core.http.netty.NettyAsyncHttpClientBuilder
import com.azure.storage.blob.*
import com.azure.storage.blob.models.*
import java.net.InetSocketAddress


object AzureClient {
//
    val connection_str = ""
    val storageClient = BlobServiceClientBuilder().connectionString(connection_str).buildClient();
    val endpoint = "https://testblobdevacc.blob.core.windows.net"
//    endpoint("https://testblobdevacc.blob.core.windows.net")
//    .sasToken(yourSasToken)
}